import datetime

from setuptools import setup, find_packages

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')


setup(
    name='mosaik.PyPower_SemVer',
    version='0.7.3' + 'rc' + TIMESTAMP,
    author='Stefan Scherfke',
    author_email='stefan.scherfke@offis.de',
    description='An adapter to use PYPOWER with mosaik.',
    long_description=(open('README.rst').read() + '\n\n' +
                      open('CHANGES.txt').read() + '\n\n' +
                      open('AUTHORS.txt').read()),
    url='https://bitbucket.org/mosaik/mosaik-pypower',
    install_requires=[
        'PYPOWER>=4.1',
        'mosaik.API-SemVer>=2.4.2rc20190716091443',
        'mosaik.Core-SemVer>=2.5.2rc20190715231038',
        'numpy>=1.6',
        'scipy>=0.9',
        'xlrd>=0.9.2,<1.2.0',
    ],
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'mosaik-pypower = mosaik_pypower.mosaik:main',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
)
